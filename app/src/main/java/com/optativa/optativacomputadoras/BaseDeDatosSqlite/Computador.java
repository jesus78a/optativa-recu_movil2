package com.optativa.optativacomputadoras.BaseDeDatosSqlite;

public class Computador {

    String nserie, descripcion, procesador,memoria, discoduro, sistemaop, puertos;
    int id, conectividad;

    public Computador() {
    }

    public Computador(String nserie, String desripcion, String procesador, String memoria, String discoduro, String sistemaop, String puertos, int id, int conectividad) {
        this.nserie = nserie;
        this.descripcion = desripcion;
        this.procesador = procesador;
        this.memoria = memoria;
        this.discoduro = discoduro;
        this.sistemaop = sistemaop;
        this.puertos = puertos;
        this.id = id;
        this.conectividad = conectividad;
    }

    public String getNserie() {
        return nserie;
    }

    public void setNserie(String nserie) {
        this.nserie = nserie;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getProcesador() {
        return procesador;
    }

    public void setProcesador(String procesador) {
        this.procesador = procesador;
    }

    public String getMemoria() {
        return memoria;
    }

    public void setMemoria(String memoria) {
        this.memoria = memoria;
    }

    public String getDiscoduro() {
        return discoduro;
    }

    public void setDiscoduro(String discoduro) {
        this.discoduro = discoduro;
    }

    public String getSistemaop() {
        return sistemaop;
    }

    public void setSistemaop(String sistemaop) {
        this.sistemaop = sistemaop;
    }

    public String getPuertos() {
        return puertos;
    }

    public void setPuertos(String puertos) {
        this.puertos = puertos;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getConectividad() {
        return conectividad;
    }

    public void setConectividad(int conectividad) {
        this.conectividad = conectividad;
    }
}
