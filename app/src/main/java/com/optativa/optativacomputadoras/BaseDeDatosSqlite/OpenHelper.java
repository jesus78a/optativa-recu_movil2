package com.optativa.optativacomputadoras.BaseDeDatosSqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class OpenHelper extends SQLiteOpenHelper {

    public OpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //Sentencia SQL que crea la tabla Computadora
        db.execSQL("create table Computador(id INTEGER PRIMARY KEY AUTOINCREMENT, nserie text, descripcion text, procesador text, conectividad int, memoria text, discoduro text, sistemaop text, puertos text)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("drop table if exists Computador");
        onCreate(db);
    }
}
